from django import forms
from .models import Profile


class LoginForm(forms.Form):
    username = forms.CharField(max_length = 200)
    password = forms.CharField(max_length = 200, widget=forms.PasswordInput)


class EditProfileForm(forms.Form):
    class Meta:
        model = Profile
        fields = ["first_name", "last_name", "email", "avatar", "phone_number"]
