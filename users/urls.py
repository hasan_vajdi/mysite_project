from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("signup", views.SignUp, name = "signup"),
    path("login", views.LoginView, name = "Login"),
    path("profile", views.profile, name = "profile"),
    path("edit_profile", views.edit_profile, name = "edit_profile"),
    path("getlike/<uuid:uid>", views.get_like_count, name = "get_like_count"),
    path("showprofile/<int:pk>", views.show_profile, name = "show_profile"),
    path("follower/<int:user_id>", views.follower, name = "follower"),
    path("get_follow_count/<int:user_id>", views.get_follow_count, name = "get_follow_count"),
    path("compelet_profile/<int:user_id>", views.compelet_profile, name = "compelet_profile"),


]
urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
