from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user            = models.ForeignKey(User, on_delete = models.CASCADE)
    first_name      = models.CharField(max_length = 10, blank = True, verbose_name = "نام")
    last_name       = models.CharField(max_length = 20, blank = True, verbose_name = "نام خانوادگی")
    email           = models.EmailField(max_length = 200, blank = True, verbose_name = "ایمیل")
    avatar          = models.ImageField(upload_to = "avatar", verbose_name = "عکس")
    phone_number    = models.CharField(max_length = 15, blank = True)
    followers       = models.ManyToManyField(User, blank = True, related_name = "follower", verbose_name = "دنبال کننده ها")
    followings      = models.ManyToManyField(User, blank = True, related_name = "following", verbose_name = "دنبال شونده ها")



    class Meta:
        verbose_name = "پروفایل"
        verbose_name_plural = "پروفایل ها"


def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user = instance)


post_save.connect(create_profile, sender = User)
