from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login
from django.views.generic.edit import UpdateView
from django.contrib.auth.forms import UserCreationForm
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from . forms import LoginForm, EditProfileForm
from . models import Profile
from infome.models import Post


def get_like_count(self, uid):
    like_count = get_object_or_404(Post, uid = uid)
    a = like_count.likes.count()
    return JsonResponse({
        "likes" : a
    })



def SignUp(request):
    form = UserCreationForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password1"]
            user_log_in = authenticate(username = username, password = password)

            login(request, user_log_in)
            return redirect("home")
        else:
            print(form.errors)

    else:
        form = UserCreationForm()

    return render(request, "registration/signup.html", {"form" : form })



def LoginView(request):
    form_login = LoginForm(request.POST)
    if form_login.is_valid():
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("home")
    else:
        form_login = LoginForm()
    return render(request, "registration/login.html", {"form" : form_login })


@login_required(redirect_field_name = "Login")
def profile(request):
    User_Information = Profile.objects.filter(user_id = request.user.id)
    post = Post.objects.filter(poster_id = request.user.id).order_by("published_date")

    if len(post) > 0:
        user_list = []
        user_list2 = []

        for j in post:
            user_list.append(get_object_or_404(User, id = j.poster_id))
        for j in user_list:
            user_list2.append(j.username)

        ziped_post = zip(user_list, post)
        context = {
            "posts" : ziped_post,
            "profile" : User_Information

            }

    else:
        context = {
            "profile" : User_Information,
        }

    return render(request, "profile.html", context)

@login_required(redirect_field_name = "Login")
def edit_profile(request):
    info = Profile.objects.filter(user_id = request.user.id)
    return render(request, "edit_profile.html", context = {"info" : info})


def show_profile(request, pk):
    User_Information = Profile.objects.filter(user_id =  pk)
    post = Post.objects.filter(poster_id = pk).order_by("published_date")

    if len(post) > 0:
        user_list = []
        user_list2 = []

        for j in post:
            user_list.append(get_object_or_404(User, id = j.poster_id))
        for j in user_list:
            user_list2.append(j.username)

        ziped_post = zip(user_list, post)
        context = {
            "posts" : ziped_post,
            "profile" : User_Information

            }
    else:
        context = {
            "profile" : User_Information,
        }

    return render(request, "show_profile.html", context)


def follower(request, user_id):
    req_user2 = get_object_or_404(Profile, user_id = request.user.id)
    users = get_object_or_404(Profile, user_id = user_id)
    user2 = get_object_or_404(User, id = user_id)

    req_user = request.user
    followers = users.followers
    req_user2_followings = req_user2.followings


    if req_user in followers.all():
        followers.remove(req_user)
        req_user2_followings.remove(user2)
    else:
        followers.add(req_user)
        req_user2_followings.add(user2)


    return redirect("show_profile", user_id)


def get_follow_count(request, user_id):
    user = get_object_or_404(Profile, user_id = user_id)
    follower_count = user.followers.count()
    following_count = user.followings.count()
    return JsonResponse({
        "followers" : follower_count,
        "followings" : following_count
    })


def compelet_profile(request, user_id):
    information = Profile.objects.get(user_id = user_id)

    avatara = "aa"
    if request.method == "POST":
        try:
            avatar = request.FILES["avatar"]
        except:
            avatar = f"{information.avatar}"

        form = request.POST
        information = Profile.objects.filter(user_id = user_id).update(first_name = form["first_name"], last_name = form["last_name"],
                                            email = form["email"], phone_number = form["phone_number"],
                                            avatar = avatar)

    #information.first_name = info["first_name"]
    #information.last_name = info["last_name"]
    #information.email = info["emial"]
    #information.avatar = info["avatar"]
        return redirect("profile")

    else:
        form = EditProfileForm()
        redirect("edit_profile")
