from django.contrib import admin

from .models import *
from django.contrib.auth.models import User




@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('poster', 'caption', 'published_date', "slug", )

@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ("user", "text", "uid",)



@admin.register(AccountEvent)
class AccountEventAdmin(admin.ModelAdmin):
    list_display = ("user_send", "user_receive", "text")
