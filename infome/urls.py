from django.urls import path

from .views import *

from django.conf.urls.static import static
from django.conf import settings



urlpatterns = [
    path('home', Home, name = "home"),
    path('addpost', AddPost1, name = "add_post1"),
    path("regpost", RegisterPost, name = "register_post"),
    path("setting", setting, name = "setting"),
    path("like/<uuid:uid>", like, name = "like"),
    path("getlike/<uuid:uid>", get_like_count, name = "get_like_count"),
    path("bookmark/<uuid:uid>", book_mark, name = "book_mark"),
    path("get_book_marks/<uuid:uid>", get_book_marks, name = "get_book_marks"),
    path("addnewcomment/<uuid:uid>", add_comment, name = "add_comment"),
    path("addcommentlike/<uuid:uid>", add_comment_like, name = "add_comment_like"),
    path("notifiction", notifiction, name = "notifiction"),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
