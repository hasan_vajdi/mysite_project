from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, JsonResponse
from .models import *
from users.models import Profile


# in this part we imported mysql and connect that
#import mysql.connector
#mydb = mysql.connector.connect(
#    user = "root",
#    host = "localhost",
#    database = "infome",
#    password = "99609970"
#)
#db = mydb.cursor()



@login_required(redirect_field_name = "Login")
def Home(request):
    post = Post.objects.all().order_by("-published_date")
    User_Information = Profile.objects.filter(user_id = request.user.id)

    if len(post) > 0:
        user_list = []
        user_list2 = []

        id_list = []

        for j in post:
            user_list.append(get_object_or_404(User, id = j.poster_id))

        for j in user_list:
            user_list2.append(j.username)
            id_list.append(j.id)

        ziped_post = zip(user_list2, post, id_list)
        context = {"post" : ziped_post, "profile" : User_Information}


    else:

        context = {"message" : "a", "profile" : User_Information}




    return render(request, "home.html", context)



@login_required(redirect_field_name = "Login")
def AddPost1(request):
    context = {}
    return render(request, "AddPost.html", context)



@login_required(redirect_field_name = "Login")
def RegisterPost(request):
    image = request.FILES["photo"]
    caption = request.POST["caption"]
    poster = request.user.id

    Add_Post_Variable = Post(
        photo = image, caption = caption,
        poster_id = poster)

    Add_Post_Variable.save()
    return redirect("home")



@login_required(redirect_field_name = "Login")
def setting(request):
    context = {}
    return render(request, "setting.html", context)



@login_required(redirect_field_name = "Login")
def like(request, uid):
    user = request.user
    post = get_object_or_404(Post, uid = uid)
    likes = post.likes.count()

    if user in post.likes.all():
        post.likes.remove(user)
    else:
        post.likes.add(user)
    return redirect("home")



@login_required(redirect_field_name = "Login")
def book_mark(request, uid):
    user = request.user
    post = get_object_or_404(Post, uid = uid)
    if user in post.book_marks.all():
        post.book_marks.remove(user)
    else:
        post.book_marks.add(user)
    return redirect("home")



def get_like_count(self, uid):
    like_count = get_object_or_404(Post, uid = uid)
    a = like_count.likes.count()
    return JsonResponse({
        "likes" : a
    })



def get_book_marks(request, uid):
    po = get_object_or_404(Post, uid = uid)
    count_book_marks = po.book_marks.count()
    return JsonResponse({"book_marks" : count_book_marks})



def post_detail(request, slug):
    context = {"post_detail" : Post.objects.filter(slug = slug)}
    return render(request, "post_detail.html", context)



def add_comment(request, uid):
    if request.method == "POST":
        post = get_object_or_404(Post, uid = uid)
        comment = post.comment_set.create(text = request.POST["comment"], user = request.user)
        return HttpResponse('')


def add_comment_like(request, uid):
    comment = get_object_or_404(Comment, uid = uid)
    user = get_object_or_404(User, id = request.GET["user_id"])
    if request.user in comment.likes.all():
        comment.likes.remove(request.user)

    else:
        comment.likes.add(request.user)
        a = AccountEvent.objects.create(
                            user_send = request.user,
                            user_receive = user,
                            text = "liked your comment"
                        )
    return HttpResponse("")


def notifiction(request):
    user = get_object_or_404(User, id = request.user.id)
    notifictions = user.accountevent_set.all()
    context = {"notif" : notifictions}
    return render(request, "notifiction.html", context)
