from django.apps import AppConfig


class InfomeConfig(AppConfig):
    name = 'infome'
    verbose_name = "پست ها"
