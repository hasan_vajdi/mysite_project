from django.db import models
from django.contrib.auth.models import User
from extensions.utils import publish_date
from django.utils import timezone
from django.template.defaultfilters import slugify
from django.conf import settings
import uuid



class Post(models.Model):
    uid                 = models.UUIDField(default = uuid.uuid4, editable = False, unique = True)
    photo               = models.ImageField(upload_to = "post", verbose_name = "عکس")
    caption             = models.CharField(max_length = 1000, verbose_name = "توضیحات")
    published_date      = models.DateTimeField(default = timezone.now, blank = True, verbose_name = "تاریخ انتشار")
    poster              = models.ForeignKey(User, on_delete = models.CASCADE, verbose_name = "کاربر", related_name = "user")
    slug                = models.SlugField(max_length = 200, unique = True)
    likes               = models.ManyToManyField(User, blank = True, verbose_name = "لایک", related_name = "like")
    book_marks          = models.ManyToManyField(User, blank = True, related_name = "book_mark", verbose_name = "بوک مارک")

    def j_publish(self):
        return publish_date(self.published_date)

    def slug(self):
        return slugify(self.uid)

    def __str__(self):
        return f"{self.poster}"

    class Meta:
        verbose_name = "اینفومی"
        verbose_name_plural = "پست"


class Comment(models.Model):
    likes   = models.ManyToManyField(User, blank = True, related_name = "liked_user")
    text    = models.CharField(max_length = 200, verbose_name = "متن")
    uid     = models.UUIDField(default = uuid.uuid4, editable = False, unique = True)
    user    = models.ForeignKey(User, on_delete = models.CASCADE, to_field = "username", verbose_name = "کاربر")
    post    = models.ForeignKey(Post, on_delete = models.CASCADE)



class AccountEvent(models.Model):
    user_send       = models.ForeignKey(User, on_delete = models.CASCADE, related_name = "user_send")
    user_receive    = models.ForeignKey(User, on_delete = models.CASCADE)
    text            = models.CharField(max_length = 200, blank = False)
