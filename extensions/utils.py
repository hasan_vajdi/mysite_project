from . import jalali


def per_num(myval):
    numb = {
        "0" : "۰",
        "1" : "۱",
        "2" : "۲",
        "3" : "۳",
        "4" : "۴",
        "5" : "۵",
        "6" : "۶",
        "7" : "۷",
        "8" : "۸",
        "9" : "۹"

    }

    for i, j in numb.items():
        myval = myval.replace(i, j)

    return myval

def publish_date(time):

    list = ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"]
    time_to_convert = f"{time.year},{time.month},{time.day}"
    shamsi = jalali.Gregorian(time_to_convert).persian_tuple()
    a = f"{shamsi[2]}  {list[shamsi[1]]} سال {shamsi[0]} ساعت {time.minute} : {time.hour}"
    return per_num(a)
